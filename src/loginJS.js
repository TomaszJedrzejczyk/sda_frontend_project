async function loginClick() {
    var inputEmail;
    inputEmail = document.getElementById("email").value;
    window.alert(inputEmail);
    var password;
    password = document.getElementById("password").value;
    var url;
    url = `http://localhost:8080/api/login?login=${inputEmail}&password=${password}`;
    console.log(url)
    // fetch(url,{method:"post"}).then(n=>console.log(n.text()));
    var respons = await fetch(url, {method: "post"});
    var message = await respons.text();
    console.log(message);
}

async function loginSubmit(form, event) {
    event.preventDefault();
    var data;
    data = new URLSearchParams(new FormData(form));
    var url;
    url = `http://localhost:8080/api/login`;
    console.log(url);
    // fetch(url,{method:"post"}).then(n=>console.log(n.text()));
    var respons = await fetch(url, {method: "post", body: data});
    var message = await respons.text();
    if (respons.status === 200) {
        window.location.href = "rejestracja.html";
    } else {
        var alert = document.getElementById("alert");
        alert.hidden = false;
        alert.innerText = message;
    }
    console.log(message);

    return false;
}